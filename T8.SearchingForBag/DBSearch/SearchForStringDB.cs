﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBSearch
{
    [Searchable]
    public class SearchForStringDB
    {
        

        public static void StringSearch(String str)
        {
            List<string> places = new List<string>();
            Console.WriteLine(new string('<',7) + "Поиск в базе данных" + new string('>', 7));
            Console.WriteLine("Введите название базы данных.");
            var DBName = Console.ReadLine();

            DBOperations dbOperations = new DBOperations();

            var connSql = dbOperations.ConnectToSQL(DBName);

            SqlCommand cmdt = new SqlCommand("SELECT* FROM INFORMATION_SCHEMA.TABLES", connSql);

            SqlDataReader reader = cmdt.ExecuteReader();

            List<string> tables = new List<string>();

            while (reader.Read())
            {
                tables.Add(reader.GetString(2));
            }
            reader.Close();


            foreach (var t in tables)
            {

                connSql = dbOperations.ConnectToSQL(DBName);
                SqlCommand cmd = new SqlCommand("Select * From " + t, connSql);

                SqlDataReader dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);

                var line = 1;

                while (dr.Read())
                {
                    for (int i = 0; i < dr.FieldCount; i++)
                    {
                        if (dr.GetValue(i).ToString().Contains(str))
                        {
                            var st = "\nЗначение найдено в таблице " + t + ",\nстолбце " + dr.GetName(i) +
                                     ",\nстроке номер " + line + "\nЗначение: " + dr.GetValue(i).ToString();

                            places.Add(st);
                        }

                    }
                    line++;
                }
                dbOperations.DisconnectSQL(connSql);
            }
            if (places.Count == 0)
            {
                places.Add("\nВ базе данных " + DBName + " не найдено ни одного значения.");
            }

            foreach (var p in places)
            {
                Console.WriteLine(p);

            }
        }
    }
}
