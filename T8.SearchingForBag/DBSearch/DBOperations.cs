﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DBSearch
{
    public class DBOperations
    {
        public bool connSuccess = true;
        public SqlConnection ConnectToSQL(string DBName)
        {
            
            
            string connStr = @"Data Source=(local)\SQLEXPRESS;
                               Initial Catalog=" + DBName +
                             ";Integrated Security=True;";

            SqlConnection conn = new SqlConnection(connStr);
            
            try
            {
                conn.Open();
            }

            catch (SqlException m)
            {
                Console.WriteLine(m + "Соединение не установлено. Введите \"Да\" чтобы попробовать еще.");
                if (Console.ReadLine() == "Да")
                {
                    ConnectToSQL(DBName);
                }
               
            }
           
            return conn;
        }

        

        

        public void DisconnectSQL(SqlConnection conn)
        {
            conn.Close();
            conn.Dispose();
        }
    }
}
