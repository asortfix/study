﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SearchInTxt
{
    
    public class FileSearching
    {
        public FileInfo[] files;

        public string DirName { get; set; }
        

        public FileInfo[] SearchFiles()
        
        {
            try
            {
                Console.WriteLine("Ввeдите директорию для поиска:");
                DirName = Console.ReadLine();
           
                DirectoryInfo dir = new DirectoryInfo(DirName);

                files = dir.GetFiles("*.txt");
                Console.WriteLine("Поиск в этих файлах:");
                foreach (var item in files)
                {
                    Console.WriteLine(item.Name);
                }
            }
            catch (ArgumentException)
            {
                Console.WriteLine("Неверно введена директория! Введите еще раз.");
                SearchFiles();
            }

            return files;
        }
    }
}
