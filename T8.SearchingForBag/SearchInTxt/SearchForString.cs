﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SearchInTxt
{
    [Searchable]
    public class SearchForString
    {
        

        public static void StringSearch(string srchValue)
        {
            List<string> succeedFiles = new List<string>();
            Console.WriteLine(new string('<', 7) + "Поиск в файлах" + new string('>', 7));
            FileSearching fs = new FileSearching();
            var files = fs.SearchFiles();
            foreach (var file in files)
            {
                var sr = file.OpenText();
                if (sr.ReadLine().Contains(srchValue))
                {
                    succeedFiles.Add(file.Name);
                }

            }
            if (succeedFiles.Count == 0)
            {
                succeedFiles.Add("\nСтрока не найдена.");
            }
            else
            {
                Console.WriteLine("\nСтрока найдена в этих файлах:");
                foreach (var sf in succeedFiles)
                {
                    Console.WriteLine(sf);
                }
            }

        }
    }
}
