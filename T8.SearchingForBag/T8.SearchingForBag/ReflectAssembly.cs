﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.IO;

namespace T8.SearchingForBag
{
    public class ReflectAssembly
    {
        List<string> assemblies = new List<string>();
        string value = ConfigurationManager.AppSettings["Attribute"];
        DirectoryInfo dir = new DirectoryInfo(AppDomain.CurrentDomain.BaseDirectory);

        public void UseSearchMethods(string srchValue)
        {


            foreach (var item in dir.GetFiles("*.dll"))
            {
                assemblies.Add(item.Name);
                
            }


            for (int i = 0; i < assemblies.Count; i++)
            {
                var path = AppDomain.CurrentDomain.BaseDirectory + assemblies[i];
                var fullName = AssemblyName.GetAssemblyName(path);

                var asm = Assembly.Load(fullName);
                var types = asm.GetTypes();

                foreach (var t in types)
                {
                    var attributes = t.GetCustomAttributes(false);

                    foreach (var attr in attributes)
                    {
                        if (attr.ToString().Contains(value))
                        {

                            t.GetMethod("StringSearch").Invoke(null, new object[] {srchValue});
                        }

                    }


                }

            }
        }

    }
}
