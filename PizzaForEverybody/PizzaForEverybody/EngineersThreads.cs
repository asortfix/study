﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PizzaForEverybody
{
    public class EngineersThreads
    {
        
        Pizza pizza = new Pizza();

        public EngineersThreads()
        {
           
        }
       
        public void ThreadFunction(object engineer)
        {
            
            OperationWithPizza owp = new OperationWithPizza();
            owp.TakePizza(pizza, (Engineers) engineer);
           
        }

    }
}
