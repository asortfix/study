﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace PizzaForEverybody
{
    class EndPizzaException : Exception
    {
        public EndPizzaException()
        {
        }

        public EndPizzaException(Engineers engineer)
        {
            string report = "Инженер №" + engineer.Number + " " + 
                                " не взял ни одного куска пиццы, т.к. пицца закончилась";
           

            Console.WriteLine(report);
            
        }

   
    }
}
