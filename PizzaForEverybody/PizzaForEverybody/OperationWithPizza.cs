﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PizzaForEverybody
{

    public class OperationWithPizza
    {
        

        private Random rand = new Random();

        private Object fileLock = new Object();

        private Object pizzaLock = new Object();



        public void TakePizza(Pizza pizza, Engineers engineer)
        {
            

                try
                {

                    for (int ii = 0; ii < 10; ii++)
                    {

                        Thread.Sleep(rand.Next(1, 1000));

                        int pieces = rand.Next(1, 3);

                        pieces = (pieces > pizza.Pieces.Count) ? 1 : pieces;

                        for (int i = 0; i < pieces; i++)
                        {

                            if (pizza.Pieces.Count == 0)
                            {
                                throw new EndPizzaException(engineer);
                            }

                            lock (pizza)
                            {
                                pizza.Pieces.RemoveAt(pizza.Pieces.Count - 1);
                            }

                        }
                        string report = "Инженер №" + engineer.Number + " " + " взял " + pieces +
                                        " кусок(куска) пиццы.";

                        lock (fileLock)
                        {
                        
                        Log.WriteReport(report);
                        
                        }

                        Console.WriteLine(report);
                    }
                }
                catch (EndPizzaException)
                {


                }
                catch (Exception msg)
                {
                    Console.WriteLine(msg);
                    TakePizza(pizza, engineer);
                }
            
        }
    }
}
