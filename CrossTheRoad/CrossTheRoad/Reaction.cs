﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;

namespace CrossTheRoad
{
    public class Reaction
    {
        public delegate void ObserveDelegate(string str);

        public static event ObserveDelegate ObserveEvent;
        

        public static void Actions()
        {

            string report;
            Random rand = new Random();


            switch ((int)TrafficLight.currentLight)
            {
                case 0:
                    foreach (var p in People.people)
                    {

                        report = (rand.Next(0, 5) != 1) ? p.Wait() : p.CrimeWalk();
                        Console.WriteLine(report);
                        ObserveEvent(report);
                    }
                    break;
                case 1:
                    foreach (var p in People.people)
                    {
                        report = (rand.Next(0, 4) != 1) ? p.GetReady() : p.CrimeWalk();
                        Console.WriteLine(report);
                        ObserveEvent(report);
                    }
                    break;

                case 2:
                    foreach (var p in People.people)
                    {
                        report = p.Move();
                        Console.WriteLine(report);
                        ObserveEvent(report);
                    }
                    break;
            }
        }
    }
}
