﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CrossTheRoad
{
    public delegate void OnChangeDelegate();

    
    public class TrafficLight
    {
       public event OnChangeDelegate OnChangeEvent;

       public static Lights currentLight;

        public enum Lights

        {
           Red,
           Yellow,
           Green
        }

        public void ChangeLight()
        {


            if ((int)currentLight < 2)
            {
                currentLight++;
            }
            else 
            {
                currentLight = 0;
            }
            OnChangeEvent();

            
        }

        public void OnChangeEventMethod()
        {
            Console.WriteLine(currentLight + "\n");
            Reaction.Actions();
            Camera.SendReport();
        }
    }
}
