﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CrossTheRoad
{
    public class Camera 
    {
        

        public static List<string> reports = new List<string>();

        public void WriteReport(string str)
        {
            reports.Add(str);
        }

        public static void SendReport()
        {
            var sw = new StreamWriter(@"D:\Report.txt",true);

            sw.WriteLine(TrafficLight.currentLight + "\n");

            foreach (var report in reports)
            {
                sw.WriteLine(report); 
                
            }
            sw.WriteLine("\n");
            reports.Clear();
            sw.Close();

        }
        
    }
}
