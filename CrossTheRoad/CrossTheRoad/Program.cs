﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace CrossTheRoad
{
    class Program
    {
        static void Main(string[] args)
        {
            People people1 = new People("Nick","Johnson");
            People people2 = new People("John","Nickolson");
            People people3 = new People("Richard","Harveyson");
            People people4 = new People("Harvey","Richardson");

            People.people.Add(people1);
            People.people.Add(people2);
            People.people.Add(people3);
            People.people.Add(people4);

            TrafficLight trafficLight1 = new TrafficLight();
            
            Camera camera1 = new Camera();

            trafficLight1.OnChangeEvent += trafficLight1.OnChangeEventMethod;

            Reaction.ObserveEvent += camera1.WriteReport;


            do
            {
                trafficLight1.ChangeLight();
                Console.WriteLine("Нажмите Enter для смены цвета светофора.\n");
            } while (Console.ReadLine() != "1");
            

            Console.Read();
        }

       
        
    }
}
