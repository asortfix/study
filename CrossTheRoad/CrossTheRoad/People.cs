﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CrossTheRoad
{
    class People
    {
        public static List<People> people = new List<People>();


      
        public string Name { get; set; }
        public string Surname { get; set; }

        public People(string name, string surname)
        {
            Name = name;
            Surname = surname;

        }
        public string Move()
        {
            string textMove = Name + " is moving...";
            return textMove;
        }

        public string Wait()
        {
            string textWait = Name + " is waiting...";
            return textWait;
        }
        public string GetReady()
        {
            string textGetReady = Name + " is getting ready...";
            return textGetReady;
        }

        public string CrimeWalk()
        {
            string textCrimeWalk = "Crime! " + Name + " run across the road!";
            return textCrimeWalk;
        }
    }
}
