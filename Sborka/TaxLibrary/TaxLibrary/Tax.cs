﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaxLibrary
{
    public class Tax
    {

        public string CountryName { get; set; }
        public double Percent { get; set; }
        public double SecondPercent { get; set; }

        public static List<Tax> Taxes = new List<Tax>();

        
        public Tax()
        {
            
        }

        public Tax(string countryName, double percent, double secondPercent)
        {
            CountryName = countryName;
            Percent = percent;
            SecondPercent = secondPercent;
        }

        public static void AddCountries()
        {
            Taxes.Add(new Tax() { CountryName = "USA", Percent = 10 });
            Taxes.Add(new Tax() { CountryName = "Ukraine", Percent = 15, SecondPercent = 3});
            Taxes.Add(new Tax() { CountryName = "Norway", Percent = 30 });
        }

        
        public static void ReturnPercent (string countryName, out double tx1, out double tx2)
        {
            var tax = Taxes.Find(tx => tx.CountryName == countryName);
            tx2 = tax.SecondPercent;
            tx1 = tax.Percent;
            

        }

        

    }
}
