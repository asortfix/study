﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using TaxLibrary;

namespace CountTaxes
{
    class Program
    {
        public static double tax1;

        public static double tax2;


        static void Main(string[] args)
        {
            var tax = new Tax();

            tax.AddCountries();

            Console.WriteLine("Введите страну (USA, Ukraine, Norway):");

            string country = Console.ReadLine();

            Console.WriteLine("Введите имя работника:");
            
            string nameWorker = Console.ReadLine();

            Console.WriteLine("Введите зарплату:");

            double salary =  double.Parse(Console.ReadLine());

           
            Tax.ReturnPercent(country, out tax1, out tax2);

            



            Console.WriteLine("{0} из {1} получает {2} баксов, так что компании надо иметь как минимум {3}.",
            nameWorker, country, salary, salary + salary*(tax1+tax2) / 100);
            Console.Read();



        }
    }
}
