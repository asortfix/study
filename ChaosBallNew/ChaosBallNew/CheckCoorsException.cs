﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace ChaosBallNew
{
    [Serializable]
    public class CheckCoorsException : Exception
    {
        public CheckCoorsException()
        {
        }

        public CheckCoorsException(string message) : base(message)
        {
        }


    }
}
