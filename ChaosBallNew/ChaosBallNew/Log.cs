﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChaosBallNew
{
    public class Log
    {

        public static List<string> Way = new List<string>();

        public static string logCoors;
        public static void GenerateLogCoors()
        {
            logCoors = "| Шар находится на координатах " + "(" + Ball.CurrXCoor + ", " + Ball.CurrYCoor + ")";

            Way.Add(logCoors);
        }

        public static StreamWriter MakeStreamWriter(string path)
        {
            StreamWriter sw = new StreamWriter(path, true);
            return sw;
        }

        public static StreamReader MakeStreamReader(string path)
        {
            StreamReader sr = new StreamReader(path);
            return sr;
        }


        public static void DeleteLog()
        {
            File.Delete(@"D:\log.txt");
            Console.WriteLine("Лог {0} удален", @"D:\log.txt");
        }


        public static void ShowAllWay()
        {
            var reader = Log.MakeStreamReader(@"D:\log.txt");
            Console.WriteLine(reader.ReadToEnd());
            reader.Close();

        }
    }
}
