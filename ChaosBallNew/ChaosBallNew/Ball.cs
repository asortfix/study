﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using ChaosBallNew;


namespace ChaosBallNew
{
    class Ball
    {
        
        //Текущие координаты шара
        public static int CurrXCoor { get; set; }
        public static int CurrYCoor { get; set; }

        

       
        
        //Передвижение шара
        public static void Move()
        {
            try
            {
                //Очистка массива пути
                Log.Way.Clear();
                Mover.Moving();
            }
            catch (CheckCoorsException msg)
            {
                Console.WriteLine(msg);
                //Вывод пути шара до столкновения в консоль

                var writer = Log.MakeStreamWriter(@"D:\log.txt");

                Console.WriteLine("--------------------Запуск!------------------------");
                writer.WriteLine("--------------------Запуск!------------------------");
                foreach (var line in Log.Way)
                {
                    writer.WriteLine(line, true);
                    Console.WriteLine(line);
                }
                writer.WriteLine("--------Возникновение исключения, остановка!-------");
                Console.WriteLine("--------Возникновение исключения, остановка!-------");
                writer.Close();
            }

            catch (OwnException msg)
            {
                 
               Console.WriteLine(msg);
            }


            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

        }
    }
}
