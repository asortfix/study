﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChaosBallNew;

namespace ChaosBallNew
{
    class Program
    {
        private static int answer;
        static void Main(string[] args)
        {

            do
            {
            Console.WriteLine("Запустить шар - введите 1\nПоказать сообщение из лога со всех запусков - введите 2\nУдалить лог - введите 3\nЗавершить программу - 4 ");
            answer = int.Parse(Console.ReadLine());
            switch (answer)
            {
                case 1:
                    Ball.Move();
                    break;
                case 2:
                    Log.ShowAllWay();
                    break;
                case 3:
                    Log.DeleteLog();
                    break;
                default:
                    answer = 4;
                    break;

            }

            } while (answer != 4);
            Console.WriteLine("Завершение программы!");
            Console.Read();
        }
    }
}
