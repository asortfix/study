﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChaosBallNew
{
    public class Mover
    {
        static Random rand = new Random();

        public static void MakeRandomCoors()
        {
            Ball.CurrXCoor = rand.Next(0, Room.borderX);
            Ball.CurrYCoor = rand.Next(0, Room.borderY);
        }

        public static void Moving()
        {
            for (int i = 1; i <= 500; i++)
            {

                MakeRandomCoors();

                Log.GenerateLogCoors();

                Console.WriteLine(Log.logCoors);

                System.Threading.Thread.Sleep(200);


                //Падение исключения в случае столкновения со стенкой
                if (Ball.CurrXCoor == 0 || Ball.CurrYCoor == 0 || Ball.CurrXCoor == Room.borderX || Ball.CurrYCoor == Room.borderY)
                {
                    throw new CheckCoorsException("Шар столкнулся со стенкой");
                }
                //Падение исключеия в случае если координата X равна координате Y
                if (Ball.CurrXCoor == Ball.CurrYCoor)
                {
                    
                    throw new OwnException("Шар передвигался по координатам до их совпадения:\n",Log.Way);
                }


            }
        }

    }
}
