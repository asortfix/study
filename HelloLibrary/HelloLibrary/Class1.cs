﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelloLibrary
{
    internal static class MagicClass
    {
        private static string hello = "Helo, ";
        private static string HelloProperty { get { return hello; } }

        private static string Hello(string name)
        {
            return HelloProperty + name;
        }

    }
}
