﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace TaskSeven.Reflection
{
    class Program
    {
        static void Main(string[] args)
        {
            //Получаю тип MagicClass
            var type = Type.GetType("HelloLibrary.MagicClass, HelloLibrary", false, true);
            Console.WriteLine(type);

            //Получаю непубличный статический метод с именем "Hello"
            var helloMethod = type.GetMethod("Hello", BindingFlags.NonPublic | BindingFlags.Static);
            Console.WriteLine(helloMethod);

            //Получаю поле "hello" и его значение
            var helloField = type.GetField("hello", BindingFlags.NonPublic | BindingFlags.Static);
            Console.WriteLine(helloField.GetValue(null));

            //Меняю значение поля "hello" и тем самым значение свойства
            helloField.SetValue(helloField, "Hello, ");

            //Получаю свойство "HelloProperty" и его значение
            var helloProperty = type.GetProperty("HelloProperty", BindingFlags.NonPublic | BindingFlags.Static);
            Console.WriteLine(helloProperty.GetValue(null));

            //Вызываю метод "Hello"
            Console.WriteLine(helloMethod.Invoke(null, new object[] { "Eugene!" }));

            Console.WriteLine(AppDomain.CurrentDomain.BaseDirectory);
            

            Console.ReadKey();
            
        }
    }
}
