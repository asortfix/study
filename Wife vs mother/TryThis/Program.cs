﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace TryThis
{
    class Program
    {


        static object knife = new object();
        static object cuttingBoard = new object();
        private static object cooking = new object();
        

        public static void WifeCooks()
        {
                lock (cooking)
                {
                    lock (knife)
                    {
                        Console.WriteLine("Wife took the knife!");
                        Console.WriteLine("Wife would like to take the cutting board......");
                        Thread.Sleep(1000);
                        lock (cuttingBoard)
                        {
                            Console.WriteLine("Wife took the cutting board!");
                            Thread.Sleep(1000);
                        }
                    }

                }
        

        }

        public static void MotherCooks()
        {

            lock (cooking)
            {
                lock (cuttingBoard)
                {
                    Console.WriteLine("Mother took the cutting board!");
                    Console.WriteLine("Mother would like to take the knife......");
                    Thread.Sleep(1000);
                    lock (knife)
                    {
                        Console.WriteLine("Mother took the knife");
                        Thread.Sleep(1000);
                    }
                }

            }
        }

 
        static void Main()
        {
            
            
            Console.WriteLine("Girls are going to start cooking\n");
            
            while (true)
            {
                new Thread((ThreadStart) WifeCooks).Start();
                new Thread((ThreadStart) MotherCooks).Start();
                Thread.Sleep(5000);
            }
        }


    }
}
