﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LibraryReload.Security
{
    public class SecurityHelper
    {
        public static string GetEmail
        {
            get { return SessionPersister.Email; }
        }
        public static bool IsAdmin
        {
            get
            {
                if (GetEmail == "admin@gmail.com")
                {
                    return true;
                }
                return false;
            }
            
        }

        public static bool IsUser
        {
            get
            {
                if (GetEmail != "admin@gmail.com"&& IsAuthorized)
                {
                    return true;
                }
                return false;
            }

        }

        public static bool IsAuthorized
        {
            get
            {
                if (GetEmail != "")
                {
                    return true;
                }
                return false;
            }

        }
    }
}