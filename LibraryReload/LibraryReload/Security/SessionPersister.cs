﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LibraryReload.Security
{
    public static class SessionPersister
    {
        private static string emailSessionvar = "email";

        public static string Email 
        {
            get
            {
                if (HttpContext.Current == null)
                {
                    return string.Empty;
                }
                var sessionVar = HttpContext.Current.Session[emailSessionvar];

                if (sessionVar != null)
                {
                    return sessionVar as string;
                }
                return null;
            }
            set
            {
                HttpContext.Current.Session[emailSessionvar] = value;
                
            }
        }
    }
}