﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using LibraryReload.DataAccess;
using LibraryReload.Models;

namespace LibraryReload.Security
{
    public class CustomAuthorizeAttribute : AuthorizeAttribute
    {
        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            if (string.IsNullOrEmpty(SecurityHelper.GetEmail))
            {
                filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new {controller = "User", action = "Logout"}));
            }
            else
            {
                UserAccess userAccess = new UserAccess();
                CustomPrincipal mp = new CustomPrincipal(userAccess.GetUserByEmail(SecurityHelper.GetEmail));
                if (!mp.IsInRole(Roles))
                {
                    filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new {controller = "Library", action = "Shelf"}));
                }
            }
        }
    }
}