﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;
using LibraryReload.DataAccess;
using LibraryReload.Models;

namespace LibraryReload.Security
{
    public class CustomPrincipal : IPrincipal
    {
        public Users User;

        public CustomPrincipal(Users user)
        {
            User = user;
            this.Identity = new GenericIdentity(user.Email);
            
        }
        public IIdentity Identity { get; set; }
        

        public bool IsInRole(string role)
        {
            var result = false;
            UserAccess userAccess = new UserAccess();

            foreach (var r in (userAccess.GetAccRoles(userAccess.GetUserId())))
            {
                foreach (var str in role.Split(','))
                {
                    if (r.Name == str)
                    {
                        result = true;
                    }
                }

            }
            return result;
        }

    }
}