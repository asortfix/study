﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.DynamicData.ModelProviders;
using System.Web.Mvc;
using System.Web.WebPages;
using LibraryReload.DataAccess;
using LibraryReload.MailSending;
using LibraryReload.Models;
using LibraryReload.Security;
using LibraryReload.ViewModels;
using PagedList;

namespace LibraryReload.Controllers
{
    public class LibraryController : Controller
    {
        // GET: Book
        public static int? QuantityAuthors;


        [CustomAuthorize(Roles = "admin")]
        public ActionResult AddBookForm()
        {

            if (QuantityAuthors == null)
            {
                ViewBag.QuantityAuthors = 2;
            }
            else { ViewBag.QuantityAuthors = QuantityAuthors;}


            return View("AddBook");
        }

        [CustomAuthorize(Roles = "admin")]
        public ActionResult AddAuthorField(int q)
        {
            q++;
            QuantityAuthors = q;
            return RedirectToAction("AddBookForm", "Library");
        }
        [CustomAuthorize(Roles = "admin")]
        public ActionResult DeleteAuthorField(int q)
        {
            if (q > 1)
            {
                q--;
            }
            QuantityAuthors = q;
            return RedirectToAction("AddBookForm", "Library");
        }

        [CustomAuthorize(Roles = "admin")]
        public ActionResult AddBook(BookViewModel bookViewModel)
        {
           
            if (ModelState.IsValid) { 
            BookAccess bookAccess = new BookAccess();
            Books book = new Books() {Available = true, Title  = bookViewModel.Book.Title, Description = bookViewModel.Book.Description, ListAuthors = bookViewModel.Book.ListAuthors,Quantity = bookViewModel.Book.Quantity};
                bookAccess.AddBook(book);
            ViewBag.Success = "The book succesfully added!";
                if (QuantityAuthors == null)
                {
                    ViewBag.QuantityAuthors = 2;
                }
                else { ViewBag.QuantityAuthors = QuantityAuthors; }
                return View();
            }
            else
            {
                if (QuantityAuthors == null)
                {
                    ViewBag.QuantityAuthors = 2;
                }
                else { ViewBag.QuantityAuthors = QuantityAuthors; }
                ViewBag.Success = "Error!";
                return View();
            }
        }



        [CustomAuthorize(Roles = "admin")]
        public ActionResult ChangeQuantity(int id, int? newq, string sortOrder, string currentFilter, string searchString, int? page, string filterRadio)
        {
            BookAccess bookAccess = new BookAccess();
            if (newq >= 0 || newq != null)
            {
                bookAccess.ChangeQuantity(id, newq);
            }

            return RedirectToAction("Shelf", new { sortOrder, currentFilter, searchString, page, filterRadio });
        }



        [CustomAuthorize(Roles = "admin,user")]
        public ActionResult Take(int id, string sortOrder, string currentFilter, string searchString, int? page, string filterRadio)
        {
            
            var action = "Take";
            BookAccess bookAccess = new BookAccess();
            UserAccess userAccess = new UserAccess();

            MailSender.SendMail(SecurityHelper.GetEmail, "Taking book", id);
            bookAccess.AddToHistory(action, userAccess.GetUserId() ,id);
            bookAccess.TakeBook(id);
            
            return RedirectToAction("Shelf",new {sortOrder,currentFilter,searchString,page,filterRadio});
        }

        [CustomAuthorize(Roles = "admin,user")]
        public ActionResult Put(int id, string sortOrder, string currentFilter, string searchString, int? page, string filterRadio)
        {
            var action = "Put";
            UserAccess userAccess = new UserAccess();
            BookAccess bookAccess = new BookAccess();

            bookAccess.AddToHistory(action, userAccess.GetUserId(), id);
            bookAccess.PutBook(id);
            return RedirectToAction("Shelf", new { sortOrder, currentFilter, searchString, page, filterRadio });
        }

        [AllowAnonymous]
        public ActionResult BookPage(int id)
        {
            BookAccess bookAccess = new BookAccess();
            var book = bookAccess.GetBookInfo(id);
            return View(book);
        }

        [CustomAuthorize(Roles = "admin")]
        public ActionResult Delete(int id, string sortOrder, string currentFilter, string searchString, int? page, string filterRadio)
        {
            BookAccess crud = new BookAccess();

            crud.DeleteBook(id);
            return RedirectToAction("Shelf", new { sortOrder, currentFilter, searchString, page, filterRadio });

        }
        [AllowAnonymous]
        public ActionResult Shelf(string sortOrder, string currentFilter, string searchString, int? page, string filterRadio)
        {
            
            BookAccess bookAccess = new BookAccess();
            var books = bookAccess.GetBooksList().AsEnumerable();

            ViewBag.Page = page;
            ViewBag.CurrentSort = sortOrder;
            ViewBag.TitleSortParm = String.IsNullOrEmpty(sortOrder) ? "Title" : "";
            ViewBag.AuthorSortParm = sortOrder == "Author" ? "AuthorDesc" : "Author";


            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewBag.CurrentFilter = searchString;

            if (!String.IsNullOrEmpty(searchString))
            {
                books = books.Where(s => s.Title.ToLower().Contains(searchString));
            }

            if (filterRadio == "available")
            {
                books = books.Where(s => s.Available);
            }

            if (filterRadio == "mybooks")
            {
                books = books.Where(s => s.CurrentOwner);
            }

            ViewBag.RadioButton = filterRadio;

            switch (sortOrder)
            {
               
                case "Title":
                    books = books.OrderByDescending(s => s.Title);
                    break;
                case "Author":
                    books = books.OrderBy(s => s.ListAuthors[0].Name);
                    break;
                case "AuthorDesc":
                    books = books.OrderByDescending(s => s.ListAuthors[0].Name);
                    
                    break;
                default:
                    books = books.OrderBy(s => s.Title);
                    break;
            }

            int pageSize = WebConfigurationManager.AppSettings["PageSize"].AsInt();
            int pageNumber = (page ?? 1);
            
            return View(books.ToPagedList(pageNumber, pageSize));
        }

    }
}