﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using LibraryReload.DataAccess;
using LibraryReload.ViewModels;
using LibraryReload.Models;
using LibraryReload.Security;


namespace LibraryReload.Controllers
{
    public class UserController : Controller
    {
        // GET: Account
        [AllowAnonymous]
        public ActionResult Index()
        {
            if (SecurityHelper.IsAuthorized == false)
            {
                return View();
            }
            else
            {
                return RedirectToAction("Shelf","Library");
            }
            
        }
        [HttpPost]
        [AllowAnonymous]
        public ActionResult Login(UserViewModel userViewModel)
        {
            UserAccess userAccess = new UserAccess();
            if (string.IsNullOrEmpty(userViewModel.User.Email)  || userAccess.Login(userViewModel.User.Email)==null)
            {
                ViewBag.Error = "Account's invalid";
                return View("Index");
                
            }
            else
            {
                SessionPersister.Email = userViewModel.User.Email;
                return RedirectToAction("Shelf", "Library");

            }
        }
        [AllowAnonymous]
        public ActionResult RegForm()
        {
            if (SecurityHelper.IsAuthorized == false)
            {
                return View("Register");
            }
            else
            {
                return RedirectToAction("Shelf", "Library");
            }
            
        }
        [AllowAnonymous]
        public ActionResult Register(UserViewModel userViewModel)
        {
            UserAccess userAccess = new UserAccess();
            
            if (ModelState.IsValid)
            {
                if (userAccess.GetUserByEmail(userViewModel.User.Email) == null)
                {
                    userAccess.AddUser(userViewModel);
                    return RedirectToAction("Index");
                }
                ViewBag.Error = "Email already exist!";
                return View();
            }
            ViewBag.Error = "Error!";
            return View();

        }
        
        public ActionResult Logout()
        {
            SessionPersister.Email = String.Empty;
            return RedirectToAction("Index");
        }
    }
}