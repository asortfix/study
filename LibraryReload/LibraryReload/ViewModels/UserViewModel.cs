﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using LibraryReload.Models;

namespace LibraryReload.ViewModels
{
    public class UserViewModel
    {
        public Users User { get; set; }
    }
}