﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Services.Description;
using LibraryReload.Models;

namespace LibraryReload.DataAccess
{
    public class BookAccess
    {

        public string connectionString = ConfigurationManager.ConnectionStrings["LibraryDB"].ConnectionString;

        public void AddBook(Books book)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand("", connection))
                {
                    connection.Open();
                    command.CommandText = "insert into Books values(@Title, @Description, @Available, @Quantity)";
                    command.Parameters.AddWithValue("@Title", book.Title);
                    command.Parameters.AddWithValue("@Description", book.Description);
                    command.Parameters.AddWithValue("@Available", true);
                    command.Parameters.AddWithValue("@Quantity", book.Quantity);
                    command.ExecuteNonQuery();

                    foreach (var author in book.ListAuthors)
                    {

                        command.CommandText = "Select * From Authors Where Name='" + author.Name + "' AND LastName='" + author.LastName + "'";
                        SqlDataReader authorReader = command.ExecuteReader();
                        if (authorReader.Read())
                        {
                            command.CommandText =
                              "insert into Books_Authors values (ident_current('Books'), " + (int)authorReader["AuthorID"] + ")";
                            authorReader.Close();
                            command.ExecuteNonQuery();
                        }

                        else 
                        {
                            authorReader.Close();
                            command.CommandText = "insert into Authors values(@Name, @LastName)";
                            command.Parameters.AddWithValue("@Name", author.Name);
                            command.Parameters.AddWithValue("@LastName", author.LastName);
                            command.ExecuteNonQuery();
                            command.Parameters.Clear();
                            command.CommandText =
                                "insert into Books_Authors values (ident_current('Books'), ident_current('Authors'))";
                            command.ExecuteNonQuery();
                        }
                        
                    }


                    
                    connection.Close();
                }

            }
        }

        public void AddToHistory(string action, int userID, int bookID)
        {

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand("", connection))
                {
                    connection.Open();
                    command.CommandText = "insert into HistoryBook values(@BookID, @UserID, @Date, @Action)";
                    command.Parameters.AddWithValue("@BookID", bookID);
                    command.Parameters.AddWithValue("@UserID", userID);
                    command.Parameters.AddWithValue("@Date", DateTime.Now);
                    command.Parameters.AddWithValue("@Action", action);
                    command.ExecuteNonQuery();
                    
                    connection.Close();
                }
            }
        }

        public Books GetBookInfo(int id)
        {
            Books book = new Books();
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand("", connection))
                {
                    connection.Open();

                    command.CommandText = "Select * From Books Where BookID=" + id;
                    SqlDataReader bookReader = command.ExecuteReader();
                    if (bookReader.Read())
                    {
                        book.Title = bookReader["Title"].ToString();
                        book.Description = bookReader["Description"].ToString();
                        book.ListAuthors = GetAuthorsOfBook(id);
                        book.History = GetBookHistory(id);
                        book.Quantity = (int) bookReader["Quantity"];
                    }
                    
                    connection.Close();
                }
            }
            return book;
        }

        public bool GetCurrentOwner(int id)
        {
            UserAccess userAccess = new UserAccess();
            var owner = false;
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand("", connection))
                {
                    connection.Open();

                    command.CommandText = "Select UserID From Users_Books Where BookID=" + id;
                    SqlDataReader userReader = command.ExecuteReader();
                    while (userReader.Read())
                    {


                        if (userAccess.GetUserId() == (int) userReader["UserID"])
                        {
                            owner = true;
                        }
                    }

                    
                    connection.Close();
                }
            }
            return owner;
        }

        public string GetUserFIOByID(int id)
        {
            string fio;
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand("", connection))
                {
                    connection.Open();

                    command.CommandText = "Select Name,LastName From Users Where UserID=" + id;
                    SqlDataReader historyReader = command.ExecuteReader();
                    historyReader.Read();
                    fio = historyReader["Name"].ToString() + " " + historyReader["LastName"].ToString();
                    
                    connection.Close();
                }
            }
            return fio;
        }

        public List<History> GetBookHistory(int id)
        {
            List<History> historyList = new List<History>();


            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand("", connection))
                {
                    connection.Open();

                    command.CommandText = "Select * From HistoryBook Where BookID=" + id;
                    SqlDataReader historyReader = command.ExecuteReader();
                    while (historyReader.Read())
                    {
                        History history = new History();
                        history.UserFIO = GetUserFIOByID((int) historyReader["UserID"]);
                        history.Date = (DateTime) historyReader["Date"];
                        history.Action = historyReader["Action"].ToString();
                        historyList.Add(history);
                    }
                    
                    connection.Close();
                }
            }
            return historyList;
        }


        
        public void TakeBook(int id)
        {
            UserAccess userAccess = new UserAccess();
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand("", connection))
                {
                    connection.Open();


                    if (GetBookInfo(id).Quantity > 0)
                    {
                        command.CommandText = "UPDATE Books Set Quantity = Quantity - 1 WHERE BookID=" + id;
                        command.ExecuteNonQuery();
                    }
                    if (GetBookInfo(id).Quantity == 0)
                    {
                        command.CommandText = "UPDATE Books Set Available = 0 WHERE BookID=" + id;
                        command.ExecuteNonQuery();
                    }
                    command.CommandText = "Insert into Users_Books values (@UserID, @BookID)";
                    command.Parameters.AddWithValue("@BookID", id);
                    command.Parameters.AddWithValue("@UserID", userAccess.GetUserId());
                    command.ExecuteNonQuery();

                    
                    connection.Close();
                }
            }
        }

        public void ChangeQuantity(int id, int? quantity)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand("", connection))
                {
                    connection.Open();
                    if (quantity >= 0)
                    {
                        command.CommandText = "UPDATE Books Set Quantity =" + quantity + " WHERE BookID=" + id;
                        command.ExecuteNonQuery();
                    }
                    
                    connection.Close();
                }
            }
        }

        public void PutBook(int id)
        {

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand("", connection))
                {
                    connection.Open();
                    command.CommandText = "UPDATE Books Set Quantity = Quantity + 1 WHERE BookID=" + id;
                    command.ExecuteNonQuery();

                    if (GetBookInfo(id).Quantity > 0)
                    {

                        command.CommandText = "UPDATE Books Set Available = 1 WHERE BookID=" + id;
                        command.ExecuteNonQuery();
                    }

                    command.CommandText = "DELETE FROM Users_Books WHERE BookID=" + id;
                    command.ExecuteNonQuery();

                    
                    connection.Close();
                }
            }
        }
        public void DeleteBook(int id)
        {
            
            string sql = string.Format("DELETE FROM Books WHERE BookID={0}", id);
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(sql, connection))
                {
                    connection.Open();


                    command.ExecuteNonQuery();

                    
                    connection.Close();
                }
            }

        }

        public List<Authors> GetAuthorsOfBook(int id)
        {
            var ListAuthors = new List<Authors>();

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand("", connection))
                {
                    connection.Open();
                    command.CommandText =
                        "select * from Authors where AuthorID in (select AuthorID from Books_Authors where BookID=" + id +
                        ")";
                    SqlDataReader authorReader = command.ExecuteReader();

                    while (authorReader.Read())
                    {
                        var author = new Authors();

                        author.Name = authorReader["Name"].ToString();
                        author.LastName = authorReader["LastName"].ToString();
                        ListAuthors.Add(author);
                    }
                    
                    connection.Close();
                }
            }
            return ListAuthors;
        }

        public List<Books> GetBooksList()
        {
            var ListBooks = new List<Books>();

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand("", connection))
                {
                    connection.Open();
                    command.CommandText = "select * from Books";
                    SqlDataReader bookReader = command.ExecuteReader();
                    while (bookReader.Read())
                    {
                        var book = new Books();
                        book.BookID = (int)bookReader["BookID"];
                        book.Title = bookReader["Title"].ToString();
                        book.Available = (bool)bookReader["Available"];
                        book.Description = bookReader["Description"].ToString();
                        book.Quantity = (int)bookReader["Quantity"];
                        book.CurrentOwner = GetCurrentOwner((int)bookReader["BookID"]);
                        book.ListAuthors = GetAuthorsOfBook(book.BookID);

                        ListBooks.Add(book);
                    }
                    
                    connection.Close();
                }
            }
            return ListBooks;
        }

        public string GetAuthorsFIOByBookID(int bookID)
        {
            var fio = "";
            BookAccess bookAccess = new BookAccess();

            foreach (var author in bookAccess.GetAuthorsOfBook(bookID))
            {
                fio += author.Name + " " + author.LastName + ", ";
            }
            return fio.Substring(0,fio.Length-2);
        }
    }
}