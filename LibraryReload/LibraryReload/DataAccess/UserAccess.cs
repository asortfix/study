﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using LibraryReload.Security;
using LibraryReload.ViewModels;
using LibraryReload.Models;

namespace LibraryReload.DataAccess
{
    public class UserAccess
    {
        
        public string connectionString = ConfigurationManager.ConnectionStrings["LibraryDB"].ConnectionString;
        

        public int GetUserId()
        {
            int id = 0;
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand("", connection))
                {
                        connection.Open();
                        command.CommandText = "select UserID from Users where Email ='" + SecurityHelper.GetEmail + "'";
                        SqlDataReader accountReader = command.ExecuteReader();

                        if (accountReader.Read())
                        {
                            id = (int) accountReader["UserID"];
                        }

                    connection.Close();
                }

            }
            return id;
        }
        public void AddUser(UserViewModel userViewModel)
        {
            var email = userViewModel.User.Email;
            var emailCheck = GetUserByEmail(email);
            var name = userViewModel.User.Name;
            var lastname = userViewModel.User.LastName;


            if (emailCheck == null)
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand command = new SqlCommand("", connection))
                    {
                        try
                        {
                            connection.Open();

                            command.CommandText = "INSERT INTO Users (Email,Name,LastName) VALUES('" + email + "','" +
                                                  name + "','" + lastname + "')";
                            command.ExecuteNonQuery();

                            command.CommandText =
                                "INSERT INTO Users_Roles (UserID,RoleID) VALUES((select ident_current('Users')),4)";
                            command.ExecuteNonQuery();

                            connection.Close();
                        }
                        catch (Exception)
                        {

                        }
                    }
                   
                }
            }
        }

        public List<Roles> GetAccRoles(int id)
        {
            List<Roles> roles = new List<Roles>();
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand("", connection))
                {


                    connection.Open();
                    command.CommandText =
                        "select * from Roles where RoleID in (select RoleID from Users_Roles where UserID=" + id +
                        ")";
                    SqlDataReader accountReader = command.ExecuteReader();


                    while (accountReader.Read())
                    {
                        var roleName = accountReader["RoleName"].ToString();
                        roles.Add(new Roles(roleName));

                    }

                    connection.Close();

                }
            }
            return roles;
            
        }

        public Users GetUserByEmail(string email)
        {
            Users user = new Users();

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand("", connection))
                {
                    connection.Open();
                    command.CommandText = "select * from Users where Email= '" + email + "'";
                    SqlDataReader accountReader = command.ExecuteReader();

                    if (accountReader.Read())
                    {
                        user.Name = accountReader["Name"].ToString();
                        user.LastName = accountReader["LastName"].ToString();
                        user.Email = accountReader["Email"].ToString();
                        user.Roles = GetAccRoles(user.UserID);
                    }

                    connection.Close();

                }
            }
            if (user.Email == email)
            {
                return user;
            }
            return null;
        }

        public Users Login(string email)
        {
            return GetUserByEmail(email);
        }

        public string  GetUserFIOByEmail(string email)
        {
            return GetUserByEmail(email).Name + " " + GetUserByEmail(email).LastName;
        }
    }
}