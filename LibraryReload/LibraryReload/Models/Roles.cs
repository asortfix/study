﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LibraryReload.Models
{
    public class Roles
    {
        public int RoleID { get; set; }
        public string Name { get; set; }

        public Roles(string name)
        {
            Name = name;
        }
    }
}