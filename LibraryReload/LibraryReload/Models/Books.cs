﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace LibraryReload.Models
{
    public class Books
    {
        
        public int BookID { get; set; }

        [Required(ErrorMessage = "The Title is required")]
        [Display(Name = "Title")]
        public string Title { get; set; }

        [Required(ErrorMessage = "The Description is required")]
        [Display(Name = "Description")]
        public string Description { get; set; }

        public bool Available { get; set; }
        public List <Authors> ListAuthors { get; set; }
        public bool CurrentOwner { get; set; }
        [Required(ErrorMessage = "The Quantity is required")]
        [Display(Name = "Quantity")]
        public int Quantity { get; set; }

        public List<History> History = new List<History>(); 

        
    }
}