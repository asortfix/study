﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace LibraryReload.Models
{
    public class Authors 
    {

        public int AuthorID { get; set; }

        [Required(ErrorMessage = "The Name is required")]
        [Display(Name = "Name")]
        public string Name { get; set; }

        [Required(ErrorMessage = "The Last Name is required")]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }
        public Books ListBooks { get; set; }


      
    }
}