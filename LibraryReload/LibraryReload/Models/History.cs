﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LibraryReload.Models
{
    public class History
    {
        public string UserFIO { get; set; }
        public int HistoryID { get; set; }
        public string BookID { get; set; }
        public string UserID { get; set; }
        public string Action { get; set; }
        public DateTime Date { get; set; }
    }
}