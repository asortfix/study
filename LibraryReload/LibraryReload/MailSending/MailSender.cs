﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Configuration;
using System.Web.Services.Description;
using System.Web.WebPages;
using LibraryReload.DataAccess;
using LibraryReload.Security;

namespace LibraryReload.MailSending
{
    public class MailSender
    {
        public static void SendMail(string email, string subject, int bookID)
        {
            var serverEmail = WebConfigurationManager.AppSettings["ServerEmail"];
            var passwordEmail = WebConfigurationManager.AppSettings["PasswordEmail"];

            using (MailMessage message = new MailMessage())
            {

                UserAccess userAccess = new UserAccess();
                BookAccess bookAccess = new BookAccess();

                message.From = new MailAddress(serverEmail);
                message.To.Add(new MailAddress(email));
                message.IsBodyHtml = true;
                message.Subject = subject;
                message.Body = "Dear " + userAccess.GetUserFIOByEmail(email) +
                             "! You have taken the book " + bookAccess.GetBookInfo(bookID).Title + " written by "
                             + bookAccess.GetAuthorsFIOByBookID(bookID) + ". Thank you for using our library!"; ;

                
                using (SmtpClient smtp = new SmtpClient("smtp.mail.ru", 25))
                {

                    smtp.EnableSsl = true;
                    smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                    smtp.UseDefaultCredentials = false;
                    smtp.Credentials = new NetworkCredential(serverEmail, passwordEmail);
                    try
                    {
                        smtp.Send(message);
                    }
                    catch 
                    {
                        
                    }

                };
               


            }


            
        }

    }
}